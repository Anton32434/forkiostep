const humburgerBtn = document.querySelector(".humburger-menu");
const humburgerLine = document.querySelector(".humburger-menu__line");
const humburgerList = document.querySelector(".nav__list");


humburgerBtn.addEventListener("click", () => {
    showList(humburgerLine, humburgerList);
})
